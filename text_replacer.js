var replace_pairs = [
		[
			'domain|domány',
			'domán'
		],
		[
			'Domain|Domány',
			'Domán'
		],
		[
			'undor',
			'andor'
		],
		[
			'Undor',
			'Andor'
		],
		[
			'first',
			'fürst'
		],
		[
			'First',
			'Fürst'
		],
		[
			'sub',
			'suba'
		],
		[
			'Sub',
			'Suba'
		]
	],
	n,
	walk = document.createTreeWalker(document.documentElement, NodeFilter.SHOW_ALL, function(node) {
		return [Node.TEXT_NODE, Node.ELEMENT_NODE].indexOf(node.nodeType) !== -1
	}, false);
while (n = walk.nextNode()) {
	replace_pairs.forEach(function(item) {
		var pattern = new RegExp(item[0], 'g');
		if (n.nodeType === Node.TEXT_NODE && n.nodeValue.length > 2) {
			text = n.nodeValue;
			if (pattern.test(text)) {
				n.nodeValue = text.replace(pattern, item[1]);
			}
		}
		else if (n.nodeType === Node.ELEMENT_NODE && n.nodeName === 'INPUT') {
			if (n.getAttribute('placeholder')) {
				text = n.getAttribute('placeholder');
				if (pattern.test(text)) {
					n.setAttribute('placeholder', text.replace(pattern, item[1]));
				}
			}
			if (n.value) {
				text = n.value;
				if (pattern.test(text)) {
					n.value = text.replace(pattern, item[1]);
				}
			}
		}
	});
}